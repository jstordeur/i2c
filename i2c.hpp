#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>
#include <stdio.h>
#include <string>

#define DEFAULT_DEV "/dev/i2c-1"

class i2cManager {
public:
	i2cManager(int addr);
	i2cManager(const char* id, int addr); //0x08

	bool openConnection();

	bool readBytes(char* buf, int length);
	bool writeBytes(char* buf, int length);




private:
	const char* device; //"/dev/i2c-1"
	int file_i2c;
	int addr;
};
