#include "i2c.hpp"

using namespace std;

i2cManager::i2cManager(int addr){
	this->device = DEFAULT_DEV;
	this->addr = addr;
}

i2cManager::i2cManager(const char* device, int addr){
	this->device = device;
	this->addr = addr;
}

bool i2cManager::openConnection() {
	if ( (this->file_i2c = open(this->device, O_RDWR)) < 0) {
		printf("Failed to open the i2c bus");
		return false;
	}

	if (ioctl(this->file_i2c, I2C_SLAVE, this->addr) < 0) {
		printf("Failed to acquire bus access and/or talk to slave.\n");
		return false;
	}

	return true;
}

bool i2cManager::readBytes(char* buf,int length) {

	if (read(this->file_i2c, buf, length) != length)
	{
		printf("Failed to read from the i2c bus.\n");
		return false;
	}

	return true;
}
bool i2cManager::writeBytes(char* buf, int length){
	if (write(this->file_i2c, buf, length) != length)
	{
		printf("Failed to write to the i2c bus.\n");
		return false;
	}

	return true;
}
